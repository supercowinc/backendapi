from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from api.v1.data.auth.models import AcUser
from rest_framework.authtoken.models import Token
import json

class AccountTests(APITestCase):
    def test_create_customer(self):
        """
        Ensure we can create a new account object.
        """
        url = '/api/v1/auth/register/'
        data = {
                'user_type': 'customer',
                'email': 'fake@fake.com',
                'password': 'fake',
                'token': 12345
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(AcUser.objects.get().email, 'fake@fake.com')
        self.assertEqual(AcUser.objects.get().user_type, 'customer')

    def test_create_courier(self):
        """
        Ensure creation of courier user_type
        """
        url = '/api/v1/auth/register/'
        data = {
            'user_type': 'courier',
            'email': 'courier@courier.com',
            'password': 'courier',
            'token': 12345
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(AcUser.objects.count(), 1)
        self.assertEqual(AcUser.objects.get().email, 'courier@courier.com')
        self.assertEqual(AcUser.objects.get().user_type, 'courier')

    def test_login_account(self):
        """
        Ensure we can login with said account that was created
        """
        url = '/api/v1/auth/login/'
        data = {
            'email': 'fake@fake.com',
            'password': 'fake'
        }
        response = self.client.get('fake@fake.com')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_customer_info(self):
        user = AcUser.objects.create()
        token = Token.objects.get(user=user)
        """
        Ensure we can enter customer information
        """
        url = '/api/v1/customers/'
        data = {
            'user': user.pk,
            'business_name': 'Fake Business Name',
            'phone_number': '512-555-5555',
            'address': 'fake street',
            'address_2': 'fake apartment',
            'city': 'Austin',
            'state': 'TX',
            'zip_code': '78758'
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token %s' % token)
        response=self.client.post(url, json.dumps(data), content_type='application/json')
        print response.data
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_courier_info(self):
        user = AcUser.objects.create()
        token = Token.objects.get(user=user)
        """
        Ensure we can enter courier information
        """
        url = '/api/v1/couriers/'
        data = {
            'user': user.pk,
            'first_name': 'Jesse',
            'last_name': 'Hodge',
            'phone_number': '512-555-5555',
            'address': 'fake street',
            'address_2': 'fake apartment',
            'city': 'Austin',
            'state': 'TX',
            'zip_code': '78758'
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token %s' % token)
        response=self.client.post(url, json.dumps(data), content_type='application/json')
        print response.data
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
