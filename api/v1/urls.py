from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from data.customer import urls as customer_urls
from data.courier import urls as courier_urls
from data.delivery import urls as delivery_urls
from data.support import urls as support_urls
from data.auth import urls as auth_urls

router = DefaultRouter()

urlpatterns = [
    url(r'^', include(auth_urls)),
    url(r'^', include(customer_urls)),
    url(r'^', include(courier_urls)),
    url(r'^', include(delivery_urls)),
    url(r'^', include(support_urls)),
]
