from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, Serializer
from django.core.validators import EmailValidator
from django.core import exceptions
from django.shortcuts import get_object_or_404
from django.contrib.auth import authenticate
from models import AcUser

USER_TYPES = (
    ( 'customer', 'Customer'),
    ( 'courier', 'Courier'),
)

class AcUserSerializer(ModelSerializer):
    user_type = serializers.CharField()

    class Meta:
        model = AcUser
        fields = ['user_type', 'email', 'password']
        write_only_fields = ['password']
        read_only_fields = ['id']

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance

class AuthCustomTokenSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            # Check if user sent email
            if EmailValidator(email):
                user_request = get_object_or_404(
                    AcUser,
                    email=email,
                )

                email = user_request.email

            user = authenticate(email=email, password=password)

            if user:
                if not user.is_active:
                    msg = ('User account is disabled.')
                    raise exceptions.ValidationError(msg)
            else:
                msg = ('Unable to log in with provided credentials.')
                raise exceptions.ValidationError(msg)
        else:
            msg = ('Must include "email" and "password"')
            raise exceptions.ValidationError(msg)

        attrs['user'] = user
        return attrs
