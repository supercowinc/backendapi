from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField
from ..auth.models import AcUser
from models import CourierProfile


class CourierProfileSerializer(ModelSerializer):
    user = PrimaryKeyRelatedField(queryset=AcUser.objects.all())

    class Meta:
        model = CourierProfile
        fields = [
            'id',
            'user',
            'first_name',
            'last_name',
            'phone_number',
            'address',
            'address_2',
            'city',
            'state',
            'zip_code',
            'license_plate',
            'make',
            'model',
            'license_number',
            'insurance_policy_number',
            'insurance_expiration_date',
        ]
        read_only_fields = ['id']
