from __future__ import unicode_literals

from django.db import models
from localflavor.us.models import PhoneNumberField, USStateField, USZipCodeField
from ..auth.models import AcUser

# Create your models here.

COURIER_STATUS_CHOICES = (
    ('Active', 'Active'),
    ('Inactive', 'Inactive'),
)

VEHICLE_MAKE_CHOICES = (
    ('buick', 'Buick'),
    ('chrysler', 'Chrysler'),
    ('chevrolet', 'Chevrolet'),
    ('cadillac', 'Cadillac'),
    ('dodge', 'Dodge'),
    ('ford', 'Ford'),
    ('gmc', 'GMC'),
    ('jeep', 'Jeep'),
    ('lincoln', 'Lincoln'),
    ('ram', 'Ram'),
    ('tesla', 'Tesla'),
)

VEHICLE_MODEL_CHOICES = ()


class CourierProfile(models.Model):
    user = models.OneToOneField(AcUser)
    first_name = models.CharField(max_length=200, null=True)
    last_name = models.CharField(max_length=200, null=True)
    phone_number = models.CharField(max_length=200, null=True)
    courier_status = models.CharField(choices=COURIER_STATUS_CHOICES, max_length=20)
    license_number = models.IntegerField(null=True)
    insurance_policy_number = models.CharField(max_length=20, null=True)
    insurance_expiration_date = models.DateField(null=True)
    # Vehicle
    license_plate = models.CharField(max_length=10, null=True)
    make = models.CharField(choices=VEHICLE_MAKE_CHOICES, null=True, blank=True, max_length=100)
    model = models.CharField(choices=VEHICLE_MODEL_CHOICES, null=True, blank=True, max_length=100)
    # Location/Address
    address = models.CharField(max_length=200, null=True)
    address_2 = models.CharField(max_length=200, null=True)
    city = models.CharField(max_length=50, null=True)
    state = USStateField(null=True)
    zip_code = USZipCodeField(null=True)

    def __unicode__(self):
        return self.user

    class Meta:
        db_table = 'courier'
        verbose_name_plural = 'couriers'
