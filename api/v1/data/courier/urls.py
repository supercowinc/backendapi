from rest_framework.routers import DefaultRouter
from views import CourierProfileViewSet

router = DefaultRouter()
router.register(r'couriers', CourierProfileViewSet, 'couriers')

urlpatterns = router.urls
