from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from serializers import CourierProfileSerializer
from models import CourierProfile

# Create your views here.


class CourierProfileViewSet(ModelViewSet):
    """
    Used for creating and retrieving couriers
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = CourierProfileSerializer
    queryset = CourierProfile.objects.all()
