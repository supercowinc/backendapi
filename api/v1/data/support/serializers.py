from rest_framework.serializers import ModelSerializer
from models import Support

class SupportSerializer(ModelSerializer):

    class Meta:
        model = Support
        fields = [
            'business_name',
            'first_name',
            'last_name',
            'email',
            'subject',
            'message'
        ]
