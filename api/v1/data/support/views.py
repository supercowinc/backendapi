from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from serializers import SupportSerializer
from models import Support

# Create your views here.


class SupportViewSet(ModelViewSet):
    """
    Used for creating and retrieving customers
    """
    serializer_class = SupportSerializer
    queryset = Support.objects.all()
