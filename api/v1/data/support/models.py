from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Support(models.Model):
    business_name = models.CharField(max_length=200)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField()
    subject = models.CharField(max_length=300)
    message = models.TextField()
