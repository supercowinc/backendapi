from rest_framework.routers import DefaultRouter
from views import SupportViewSet

router = DefaultRouter()
router.register(r'support', SupportViewSet, 'support')

urlpatterns = router.urls
