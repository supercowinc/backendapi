from __future__ import unicode_literals

from django.db import models
from localflavor.us.models import PhoneNumberField, USStateField, USZipCodeField
from ..auth.models import AcUser

# Create your models here.


class CustomerProfile(models.Model):
    user = models.OneToOneField(AcUser, primary_key=True)
    business_name = models.CharField(max_length=200, null=True)
    # Location/Address
    address = models.CharField(max_length=200, null=True)
    address_2 = models.CharField(max_length=200, null=True)
    city = models.CharField(max_length=50, null=True)
    state = USStateField(null=True)
    zip_code = USZipCodeField(null=True)
    # Phone
    phone_number = PhoneNumberField(max_length=100, null=True)

    def __unicode__(self):
        return self.business_name

    class Meta:
        db_table = 'customer'
        verbose_name_plural = 'customers'
