from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField
from ..auth.models import AcUser
from models import CustomerProfile


class CustomerProfileSerializer(ModelSerializer):
    user = PrimaryKeyRelatedField(queryset=AcUser.objects.all())

    class Meta:
        model = CustomerProfile
        fields = [
            'user',
            'business_name',
            'phone_number',
            'address',
            'address_2',
            'city',
            'state',
            'zip_code'
        ]
'''
    def update(self, validated_data, instance):
        """
        Update and return an existing `CustomerProfile` instance, given the validated data.
        """
        instance.user = validated_data.get('user', instance.user)
        instance.business_name = validated_data.get('business_name', instance.business_name)
        instance.phone_number = validated_data.get('phone_number', instance.phone_number)
        instance.address = validated_data.get('address', instance.address)
        instance.address_2 = validated_data.get('address_2', instance.address_2)
        instance.city = validated_data.get('city', instance.city)
        instance.state = validated_data.get('state', instance.state)
        instance.zip_code = validated_data.get('zip_code', instance.zip_code)
        instance.save()
        return instance
'''
