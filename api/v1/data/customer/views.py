from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from serializers import CustomerProfileSerializer
from models import CustomerProfile

# Create your views here.


class CustomerProfileViewSet(ModelViewSet):
    """
    Used for creating and retrieving customers
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = CustomerProfileSerializer
    queryset = CustomerProfile.objects.all()


from rest_framework import generics

class CustomerInfo(generics.ListAPIView):
    serializer_class = CustomerProfileSerializer

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        user = self.kwargs['user']
        return CustomerProfile.objects.filter(user=user)
