from rest_framework.routers import DefaultRouter
from django.conf.urls import url
from views import CustomerProfileViewSet, CustomerInfo

router = DefaultRouter()
router.register(r'customers', CustomerProfileViewSet, base_name='customers')
urlpatterns = [
    url('^customer_info/(?P<user>.+)/$', CustomerInfo.as_view()),
]

urlpatterns += router.urls
