from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics
from serializers import DeliverySerializer
from models import Delivery

# Create your views here.


class DeliveryViewSet(ModelViewSet):
    """
    Viewset for listing and retrieving deliveries
    """
    permission_classes = (IsAuthenticated,)
    queryset = Delivery.objects.all()
    serializer_class = DeliverySerializer


class CustomerDeliveryList(ModelViewSet):
    serializer_class = DeliverySerializer

    def get_queryset(self):
        user = self.request.user
        return Delivery.objects.filter(customer=user)
