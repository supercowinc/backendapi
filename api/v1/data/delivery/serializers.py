from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField
from ..customer.models  import CustomerProfile
from models             import Delivery


class DeliverySerializer(ModelSerializer):
    customer = PrimaryKeyRelatedField(queryset=CustomerProfile.objects.all())

    class Meta:
        model = Delivery
        fields = [
            'id',
            'customer',
            'status',
            'pickup_business_name',
            'pickup_phone_number',
            'pickup_address',
            'pickup_address2',
            'pickup_city',
            'pickup_state',
            'pickup_zipCode',
            'dropoff_business_name',
            'dropoff_phone_number',
            'dropoff_address',
            'dropoff_address2',
            'dropoff_city',
            'dropoff_state',
            'dropoff_zipCode'
        ]
        read_only_fields = ['id']
