from rest_framework.routers import DefaultRouter
from django.conf.urls import url
from views import DeliveryViewSet, CustomerDeliveryList

router = DefaultRouter()
router.register(r'customer_deliveries', CustomerDeliveryList, 'customer_deliveries')
router.register(r'deliveries', DeliveryViewSet, 'deliveries')

urlpatterns = router.urls
