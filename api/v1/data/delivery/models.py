from __future__ import unicode_literals

from django.db import models
from ..customer.models import CustomerProfile

# Create your models here.

DELIVERY_STATUS_CHOICES = (
    ('Scheduled', 'Scheduled'),
)


class Delivery(models.Model):
    customer = models.ForeignKey(CustomerProfile)
    created_at = models.DateField(auto_now=True)
    status = models.CharField(choices=DELIVERY_STATUS_CHOICES, max_length=32)
    pickup_business_name = models.CharField(max_length=200)
    pickup_phone_number = models.CharField(max_length=200)
    pickup_address = models.CharField(max_length=200)
    pickup_address2 = models.CharField(max_length=200)
    pickup_city = models.CharField(max_length=200)
    pickup_state = models.CharField(max_length=200)
    pickup_zipCode = models.CharField(max_length=200)
    dropoff_business_name = models.CharField(max_length=200)
    dropoff_phone_number = models.CharField(max_length=200)
    dropoff_address = models.CharField(max_length=200)
    dropoff_address2 = models.CharField(max_length=200)
    dropoff_city = models.CharField(max_length=200)
    dropoff_state = models.CharField(max_length=200)
    dropoff_zipCode = models.CharField(max_length=200)

    def __unicode__(self):
        return self.status

    class Meta:
        db_table = 'delivery'
        verbose_name_plural = 'deliveries'
