from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from v1 import urls as v1_urls

router = DefaultRouter()

urlpatterns = [
    url(r'^v1/', include(v1_urls)),
    url(r'^docs/', include('rest_framework_docs.urls')),
]
